package com.epam.view;

import com.epam.menu.View;

public class Main {
  public static void main(String[] args) {
    new View().showMenu();
  }
}
