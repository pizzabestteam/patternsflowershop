package com.epam.manage;

import com.epam.shopnetwork.RealShop;
import com.epam.utils.Utilits;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

import static com.epam.cards.Cards.NONE;

public class ClientManager {

  private RealShop realShop;
  private Utilits utilits;
  private static Logger logger = LogManager.getLogger(ClientManager.class);
  private static Scanner input = new Scanner(System.in);

  public ClientManager(){
    realShop = new RealShop();
    utilits = new Utilits();
  }

  public void identifyClient() {
    logger.trace("Do you have any card ? enter : y/n");
    String card;
    String answer = input.nextLine();
    if(answer.equalsIgnoreCase("y")){
      logger.trace("Enter type of your Card");
      card = input.nextLine();
      logger.trace("Choose your shop");
      answer = input.nextLine();
      realShop.enterToShop(answer,utilits.getCardsByString(card));
    }
    if(answer.equalsIgnoreCase("n")){
      logger.trace("Choose shop Bandery shop, Horodotska shop, Franka shop");
      answer = input.nextLine();
      realShop.enterToShop(answer,NONE);
    }
  }
}
