package com.epam.shopnetwork.order.bouquet.implementbouquet;

import com.epam.shopnetwork.order.FlowerTypes;
import com.epam.shopnetwork.order.bouquet.Bouquet;
import com.epam.utils.Utilits;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import static com.epam.shopnetwork.order.FlowerTypes.CHAMOMILE;
import static com.epam.shopnetwork.order.FlowerTypes.CORNFLOWER;
import static com.epam.shopnetwork.order.FlowerTypes.ROSE;

public class ValentineBouquet implements Bouquet {
  private static Logger logger = LogManager.getLogger(BirthdayBouquet.class);
  private static Scanner input = new Scanner(System.in);
  private Utilits utilits = new Utilits();
  Map<String, Integer> flowers = new LinkedHashMap<>();
  private static double FLOWER_PRICE = 0.5;

  @Override
  public void addFlowers() {
    String flower;
    Integer number;

    do {
      logger.trace("Enter flowers what you need");
      flower = input.nextLine();
      number = 10;
      FLOWER_PRICE = FLOWER_PRICE * 10;
      logger.trace("Add else flower");
      if (!flower.equalsIgnoreCase("Q") && ROSE.equals(utilits.getFlowerByString(flower))) {
        flowers.put("Rose", number);
      }
      if (!flower.equalsIgnoreCase("Q") && CORNFLOWER.equals(utilits.getFlowerByString(flower))) {
        flowers.put("Cornflower", number);
      }
      if (!flower.equalsIgnoreCase("Q") && CHAMOMILE.equals(utilits.getFlowerByString(flower))) {
        flowers.put("Chamomile", number);
      }
    } while (!flower.equalsIgnoreCase("Q"));
  }

  @Override
  public String getFlowers() {
    String result = flowers.entrySet()
            .stream()
            .map(entry -> entry.getKey() + " - " + entry.getValue())
            .collect(Collectors.joining(", "));
    return result;
  }

  @Override
  public double getPrice() {
    return FLOWER_PRICE;
  }

  @Override
  public String toString() {
    return "BirthdayBouquet{" +
            "flowers=" + flowers +
            '}';
  }
}
