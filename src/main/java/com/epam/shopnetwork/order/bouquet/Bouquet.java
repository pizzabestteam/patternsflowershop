package com.epam.shopnetwork.order.bouquet;

import com.epam.shopnetwork.extraoptions.Options;

public interface Bouquet extends Options {

  void addFlowers();

  String getFlowers();
}
