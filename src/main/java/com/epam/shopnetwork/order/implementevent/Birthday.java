package com.epam.shopnetwork.order.implementevent;

import com.epam.shopnetwork.order.Event;
import com.epam.shopnetwork.order.EventType;
import com.epam.shopnetwork.order.bouquet.Bouquet;
import com.epam.shopnetwork.order.bouquet.implementbouquet.BirthdayBouquet;

import static com.epam.shopnetwork.order.EventType.BIRTHDAY_BOUQUET;

public class Birthday extends Event {

  @Override
  public Bouquet chooseBouquetType(EventType bouquetType) {
    Bouquet bouquet = null;
    if (bouquetType.equals(BIRTHDAY_BOUQUET)) {
      bouquet = new BirthdayBouquet();
      return bouquet;
    }
    return bouquet;
  }
}
