package com.epam.shopnetwork.order.implementevent;

import com.epam.shopnetwork.order.Event;
import com.epam.shopnetwork.order.EventType;
import com.epam.shopnetwork.order.bouquet.Bouquet;
import com.epam.shopnetwork.order.bouquet.implementbouquet.WeddingBouquet;

import static com.epam.shopnetwork.order.EventType.WEDDING_BOUQUET;

public class Wedding extends Event {
  @Override
  public Bouquet chooseBouquetType(EventType bouquetType) {
    Bouquet bouquet = null;
    if (bouquetType.equals(WEDDING_BOUQUET) ) {
      bouquet = new WeddingBouquet();
      return bouquet;
    }
    return bouquet;
  }
}
