package com.epam.shopnetwork.order.implementevent;

import com.epam.shopnetwork.order.Event;
import com.epam.shopnetwork.order.EventType;
import com.epam.shopnetwork.order.bouquet.Bouquet;
import com.epam.shopnetwork.order.bouquet.implementbouquet.ValentineBouquet;

import static com.epam.shopnetwork.order.EventType.VALENTINE_BOUQUET;

public class Valentine extends Event {
  @Override
  public Bouquet chooseBouquetType(EventType bouquetType) {
    Bouquet bouquet = null;
    if (bouquetType.equals(VALENTINE_BOUQUET) /*&& sizeType.equals(BIG)*/) {
      bouquet = new ValentineBouquet();
      return bouquet;
    }
    return bouquet;
  }
}
