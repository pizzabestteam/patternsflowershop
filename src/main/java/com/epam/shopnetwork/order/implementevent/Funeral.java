package com.epam.shopnetwork.order.implementevent;

import com.epam.shopnetwork.order.Event;
import com.epam.shopnetwork.order.EventType;
import com.epam.shopnetwork.order.bouquet.Bouquet;
import com.epam.shopnetwork.order.bouquet.implementbouquet.FuneralBouquet;

import static com.epam.shopnetwork.order.EventType.FUNERAL_BOUQUET;

public class Funeral extends Event {

  @Override
  public Bouquet chooseBouquetType(EventType bouquetType) {
    Bouquet bouquet = null;
    if (bouquetType.equals(FUNERAL_BOUQUET) /*&& sizeType.equals(BIG)*/) {
      bouquet = new FuneralBouquet();
      return bouquet;
    }
    return bouquet;
  }
}
