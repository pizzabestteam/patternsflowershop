package com.epam.shopnetwork.order;

import com.epam.shopnetwork.order.bouquet.Bouquet;

public abstract class Event {
  public abstract Bouquet chooseBouquetType(EventType bouquetType);

  public  Bouquet createBouquet(EventType bouquetType){
    Bouquet bouquet = chooseBouquetType(bouquetType);
    bouquet.addFlowers();
    return bouquet;
  }
}
