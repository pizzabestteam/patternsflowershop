package com.epam.shopnetwork.messages;

public interface Subscriber {
  void update();
}
