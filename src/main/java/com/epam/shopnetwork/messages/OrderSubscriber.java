package com.epam.shopnetwork.messages;

public class OrderSubscriber implements Subscriber {

  private Publisher publisher;

  public OrderSubscriber(Publisher publisher) {
    this.publisher = publisher;
    this.publisher.setObservers(this);
  }

  @Override
  public void update() {
    System.out.println("ready");
  }
}
