package com.epam.shopnetwork.messages;

import java.util.ArrayList;
import java.util.List;

public class Publisher {

  private List<Subscriber> observersList = new ArrayList<>();
  private Boolean state;
  private Double bonus;

  public Double getBonus() {
    return bonus;
  }

  public void setBonus(Double bonus) {
    this.bonus = bonus;
    notifyObserver();
  }

  public Boolean getState() {
    return state;
  }

  public void setState(Boolean state) {
    this.state = state;
    notifyObserver();
  }

  public void setObservers(Subscriber observers) {
    observersList.add(observers);
  }

  private void notifyObserver() {
    for (Subscriber subscriber : observersList) {
      subscriber.update();
    }
  }
}
