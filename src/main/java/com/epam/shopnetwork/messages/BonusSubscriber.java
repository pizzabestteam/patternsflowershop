package com.epam.shopnetwork.messages;

public class BonusSubscriber implements Subscriber {

  private Publisher publisher;

  public BonusSubscriber(Publisher publisher) {
    this.publisher = publisher;
    this.publisher.setObservers(this);
  }

  @Override
  public void update() {
    System.out.println("You have bonus " + this.publisher.getBonus());
  }
}
