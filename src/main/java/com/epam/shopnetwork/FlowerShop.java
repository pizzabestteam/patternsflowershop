package com.epam.shopnetwork;

import com.epam.cards.Cards;

public interface FlowerShop {
  void enterToShop(String shop, Cards cardsByString);
}
