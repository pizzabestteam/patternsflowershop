package com.epam.shopnetwork;

import com.epam.cards.Cards;
import com.epam.shopnetwork.messages.BonusSubscriber;
import com.epam.shopnetwork.messages.OrderSubscriber;
import com.epam.shopnetwork.messages.Publisher;
import com.epam.shopnetwork.shops.BanderyShop;
import com.epam.shopnetwork.shops.FrankaShop;
import com.epam.shopnetwork.shops.HorodotskaShop;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class RealShop implements FlowerShop {

  private BanderyShop banderyShop;
  private FrankaShop frankaShop;
  private HorodotskaShop horodotskaShop;
  private static Logger logger = LogManager.getLogger(RealShop.class);
  private static Scanner input = new Scanner(System.in);

  public void enterToShop(String shop, Cards cardsByString) {

    if (shop.equalsIgnoreCase("Bandery")) {
      logger.trace("Custom order - press 1 or Catalog order press 2: ");
      Integer choose = input.nextInt();
      banderyShop = new BanderyShop();
      if (choose.equals(1)) {
        banderyShop.customOrder(cardsByString);
        Publisher publisher = new Publisher();
        new BonusSubscriber(publisher);
        publisher.setBonus(10.0);
      }
      if (choose.equals(2)) {
        banderyShop.catalogOrder(cardsByString);
      }
      Publisher publisher = new Publisher();
      new OrderSubscriber(publisher);
      publisher.setState(true);
    }
    if (shop.equalsIgnoreCase("Franka")) {
      frankaShop = new FrankaShop();
      logger.trace("Custom order - press 1 or Catalog order press 2: ");
      Integer choose = input.nextInt();
      if (choose.equals(1)) {
        frankaShop.customOrder(cardsByString);
        Publisher publisher = new Publisher();
        new BonusSubscriber(publisher);
        publisher.setBonus(10.0);
      }
      if (choose.equals(2)) {
        frankaShop.catalogOrder(cardsByString);
      }
    }
    if (shop.equalsIgnoreCase("Horodotska")) {
      horodotskaShop = new HorodotskaShop();
      logger.trace("Custom order - press 1 or Catalog order press 2: ");
      Integer choose = input.nextInt();
      if (choose.equals(1)) {
        horodotskaShop.customOrder(cardsByString);
      }
      if (choose.equals(2)) {
        horodotskaShop.catalogOrder(cardsByString);
        Publisher publisher = new Publisher();
        new OrderSubscriber(publisher);
        publisher.setState(true);
      }
    }
  }
}
