package com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.discount;

import com.epam.shopnetwork.extraoptions.concreateoptions.BouquetDecorator;

public class SilverDiscount extends BouquetDecorator {

  private static final double SILVER_DISCOUNT = -20;

  public SilverDiscount() {
    setOptionPrice(SILVER_DISCOUNT);
  }
}
