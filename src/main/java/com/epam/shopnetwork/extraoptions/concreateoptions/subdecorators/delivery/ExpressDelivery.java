package com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.delivery;

import com.epam.shopnetwork.extraoptions.concreateoptions.BouquetDecorator;

public class ExpressDelivery extends BouquetDecorator {
  private final double EXPRESS_DELIVERY_PRICE = 5;

  public ExpressDelivery() {
    setOptionPrice(EXPRESS_DELIVERY_PRICE);
  }
}
