package com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.packaging;

import com.epam.shopnetwork.extraoptions.concreateoptions.BouquetDecorator;
import com.epam.shopnetwork.order.EventType;

import static com.epam.shopnetwork.order.EventType.*;

public class Box extends BouquetDecorator {

  private final double PACKAGING_BOX_PRICE = 5;
  private final String RED = "Red";
  private final String BLACK = "Black";
  private final String PINK = "Pink";
  private final String WHITE = "White";

  public Box(EventType eventType) {
    if (eventType.equals(BIRTHDAY_BOUQUET)) {
      setOptionColor(RED);
    }
    if (eventType.equals(FUNERAL_BOUQUET)) {
      setOptionColor(BLACK);
    }
    if (eventType.equals(VALENTINE_BOUQUET)) {
      setOptionColor(PINK);
    }
    if (eventType.equals(WEDDING_BOUQUET)) {
      setOptionColor(WHITE);
    }
    setOptionPrice(PACKAGING_BOX_PRICE);
  }
}
