package com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.discount;

import com.epam.shopnetwork.extraoptions.concreateoptions.BouquetDecorator;

public class GoldDiscount extends BouquetDecorator {
  private final double GOLD_DISCOUNT = -50;

  public GoldDiscount() {
    setOptionPrice(GOLD_DISCOUNT);
  }
}
