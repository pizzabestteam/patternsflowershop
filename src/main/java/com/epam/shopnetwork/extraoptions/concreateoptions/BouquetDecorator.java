package com.epam.shopnetwork.extraoptions.concreateoptions;

import com.epam.shopnetwork.extraoptions.Options;

import java.util.Optional;

public class BouquetDecorator implements Options {

  private Optional<Options> bouquet;
  private Double optionsPrice;
  private String color = " ";

  public void decorate(Options options) {
    bouquet = Optional.ofNullable(options);
  }

  public void setOptionPrice(Double optionPrice) {
    this.optionsPrice = optionPrice;
  }

  public void setOptionColor(String optionColor) {
    this.color = optionColor;
  }

  @Override
  public double getPrice() {
    return bouquet.orElseThrow(IllegalArgumentException::new).getPrice() + optionsPrice;
  }

}
