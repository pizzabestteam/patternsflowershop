package com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.discount;

import com.epam.shopnetwork.extraoptions.concreateoptions.BouquetDecorator;

public class DefaultDiscount extends BouquetDecorator {
  private final double DEFAULT = 2;

  public DefaultDiscount() {
    setOptionPrice(DEFAULT);
  }
}
