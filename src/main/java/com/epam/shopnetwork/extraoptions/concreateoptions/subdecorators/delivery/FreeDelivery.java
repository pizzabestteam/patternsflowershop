package com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.delivery;

import com.epam.shopnetwork.extraoptions.concreateoptions.BouquetDecorator;

public class FreeDelivery extends BouquetDecorator {

  public FreeDelivery() {
    setOptionPrice(0.0);
  }
}
