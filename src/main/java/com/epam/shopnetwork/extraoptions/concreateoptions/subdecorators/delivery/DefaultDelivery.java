package com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.delivery;

import com.epam.shopnetwork.extraoptions.concreateoptions.BouquetDecorator;

public class DefaultDelivery extends BouquetDecorator {
  private final double DEFAULT_DELIVERY_PRICE = 2;

  public DefaultDelivery() {
    setOptionPrice(DEFAULT_DELIVERY_PRICE);
  }
}
