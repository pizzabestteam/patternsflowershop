package com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.packaging;

import com.epam.shopnetwork.extraoptions.concreateoptions.BouquetDecorator;

public class DefaultPackaging extends BouquetDecorator {
  private final double DEFAULT_PACKAGING_PRICE = 2;

  public DefaultPackaging() {
    setOptionPrice(DEFAULT_PACKAGING_PRICE);
  }
}
