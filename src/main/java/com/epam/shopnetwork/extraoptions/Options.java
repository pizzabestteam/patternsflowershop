package com.epam.shopnetwork.extraoptions;

public interface Options {
  double getPrice();
}
