package com.epam.shopnetwork.extraoptions.implement;

import com.epam.shopnetwork.extraoptions.Options;

public class DeliveryImpl  implements Options {

  private final double DELIVERY_PACKAGING = 2;

  @Override
  public double getPrice() {
    return DELIVERY_PACKAGING;
  }
}
