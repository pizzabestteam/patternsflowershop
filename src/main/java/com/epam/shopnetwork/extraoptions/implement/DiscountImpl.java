package com.epam.shopnetwork.extraoptions.implement;

import com.epam.shopnetwork.extraoptions.Options;

public class DiscountImpl implements Options {

  private final double DISCOUNT_PACKAGING = 2;

  @Override
  public double getPrice() {
    return DISCOUNT_PACKAGING;
  }
}
