package com.epam.shopnetwork.extraoptions.implement;

import com.epam.shopnetwork.extraoptions.Options;

public class PackagingImpl implements Options {

  private final double PRICE_PACKAGING = 2;

  @Override
  public double getPrice() {
    return PRICE_PACKAGING;
  }

}
