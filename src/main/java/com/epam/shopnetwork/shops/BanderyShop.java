package com.epam.shopnetwork.shops;

import com.epam.cards.Cards;
import com.epam.shopnetwork.extraoptions.concreateoptions.BouquetDecorator;
import com.epam.shopnetwork.order.Event;
import com.epam.shopnetwork.order.bouquet.Bouquet;
import com.epam.utils.Utilits;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class BanderyShop {

  private Utilits utilits = new Utilits();
  private static Logger logger = LogManager.getLogger(BanderyShop.class);
  private static Scanner input = new Scanner(System.in);
  private Map<Integer, String> catalog = new LinkedHashMap<>();

  public BanderyShop() {
    createCatalog();
  }

  private void createCatalog() {

  }

  public void customOrder(Cards cardsByString) {
    logger.trace("Enter type of event: ");
    String type = input.nextLine();
    Event objectEvent = utilits.getEventTypeObject(utilits.getEventType(type));
    Bouquet bouquet = objectEvent.createBouquet(utilits.getEventType(type));
    logger.trace("Enter type of packaging: ");
    type = input.nextLine();
    BouquetDecorator packaging = utilits.getPackaging(type, utilits.getEventType(type));
    BouquetDecorator delivery = utilits.getDelivery(cardsByString);
    BouquetDecorator discount = utilits.getDiscount(cardsByString);
    packaging.decorate(bouquet);
    delivery.decorate(packaging);
    discount.decorate(delivery);
    logger.trace("Flowers :" + bouquet.getFlowers());
    logger.trace("Bouquet price :" + bouquet.getPrice());
    logger.trace("Additional operation price :" + discount.getPrice());
  }

  public void catalogOrder(Cards cardsByString) {

  }
}
