package com.epam.shopnetwork.shops;

import com.epam.cards.Cards;
import com.epam.shopnetwork.extraoptions.concreateoptions.BouquetDecorator;
import com.epam.shopnetwork.order.Event;
import com.epam.shopnetwork.order.bouquet.Bouquet;
import com.epam.utils.Utilits;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class HorodotskaShop {
  private Utilits utilits = new Utilits();
  private static Logger logger = LogManager.getLogger(BanderyShop.class);
  private static Scanner input = new Scanner(System.in);

  public void customOrder(Cards cardsByString) {
    logger.trace("Enter type of event: ");
    String type = input.nextLine();
    Event objectEvent = utilits.getEventTypeObject(utilits.getEventType(type));
    Bouquet bouquet = objectEvent.createBouquet(utilits.getEventType(type));
    logger.trace("Enter type of packaging: ");
    type = input.nextLine();
    BouquetDecorator packaging = utilits.getPackaging(type, utilits.getEventType(type));
    BouquetDecorator discount = utilits.getDiscount(cardsByString);
    BouquetDecorator delivery = utilits.getDelivery(cardsByString);
    packaging.decorate(bouquet);
    discount.decorate(packaging);
    delivery.decorate(discount);
    logger.trace("Flowers :" + bouquet.getFlowers());
    logger.trace("Bouquet price :" + bouquet.getPrice());
    logger.trace("Additional operation price :" + discount.getPrice());
  }

  public void catalogOrder(Cards cardsByString) {

  }
}
