package com.epam.utils;

import com.epam.cards.Cards;
import com.epam.shopnetwork.extraoptions.concreateoptions.BouquetDecorator;
import com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.delivery.DefaultDelivery;
import com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.delivery.ExpressDelivery;
import com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.delivery.FreeDelivery;
import com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.discount.DefaultDiscount;
import com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.discount.GoldDiscount;
import com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.discount.SilverDiscount;
import com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.packaging.Box;
import com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.packaging.DefaultPackaging;
import com.epam.shopnetwork.extraoptions.concreateoptions.subdecorators.packaging.Tape;
import com.epam.shopnetwork.order.Event;
import com.epam.shopnetwork.order.EventType;
import com.epam.shopnetwork.order.FlowerTypes;
import com.epam.shopnetwork.order.implementevent.Birthday;
import com.epam.shopnetwork.order.implementevent.Funeral;
import com.epam.shopnetwork.order.implementevent.Valentine;
import com.epam.shopnetwork.order.implementevent.Wedding;

import static com.epam.cards.Cards.*;
import static com.epam.shopnetwork.order.EventType.*;
import static com.epam.shopnetwork.order.FlowerTypes.CHAMOMILE;
import static com.epam.shopnetwork.order.FlowerTypes.CORNFLOWER;
import static com.epam.shopnetwork.order.FlowerTypes.ROSE;

public class Utilits {

  public Cards getCardsByString(String stringCard) {
    if (stringCard.equalsIgnoreCase("GOLD")) {
      return GOLD_CARD;
    }
    if (stringCard.equalsIgnoreCase("BONUS")) {
      return BONUS_CARD;
    }
    if (stringCard.equalsIgnoreCase("SOCIAL")) {
      return SOCIAL_CARD;
    }
    return NONE;
  }

  public FlowerTypes getFlowerByString(String flower) {
    if (flower.equalsIgnoreCase("ROSE")) {
      return ROSE;
    }
    if (flower.equalsIgnoreCase("CHAMOMILE")) {
      return CHAMOMILE;
    }
    if (flower.equalsIgnoreCase("CORNFLOWER")) {
      return CORNFLOWER;
    }
    return null; //delete
  }

  public BouquetDecorator getDiscount(Cards cardsByString) {
    if (cardsByString.equals(SOCIAL_CARD)) {
      return new GoldDiscount();
    }
    if (cardsByString.equals(BONUS_CARD)) {
      return new SilverDiscount();
    }
    return new DefaultDiscount();
  }

  public BouquetDecorator getDelivery(Cards cardsByString) {
    if (cardsByString.equals(GOLD_CARD)) {
      return new FreeDelivery();
    }
    if (cardsByString.equals(BONUS_CARD)) {
      return new ExpressDelivery();
    }
    return new DefaultDelivery();
  }

  public EventType getEventType(String type) {
    if (type.equalsIgnoreCase("Birthday")) {
      return BIRTHDAY_BOUQUET;
    }
    if (type.equalsIgnoreCase("Funeral")) {
      return FUNERAL_BOUQUET;
    }
    if (type.equalsIgnoreCase("Valentine")) {
      return VALENTINE_BOUQUET;
    }
    if (type.equalsIgnoreCase("Wedding")) {
      return WEDDING_BOUQUET;
    }
    return EVERY_DATE;
  }

  public Event getEventTypeObject(EventType type) {
    if (type.equals(BIRTHDAY_BOUQUET)) {
      return new Birthday();
    }
    if (type.equals(FUNERAL_BOUQUET)) {
      return new Funeral();
    }
    if (type.equals(VALENTINE_BOUQUET)) {
      return new Valentine();
    }
    if (type.equals(WEDDING_BOUQUET)) {
      return new Wedding();
    }
    return null;
  }

  public BouquetDecorator getPackaging(String type, EventType eventType) {
    if (type.equalsIgnoreCase("Box")) {
      return new Box(eventType);
    }
    if (type.equalsIgnoreCase("Tape")) {
      return new Tape(eventType);
    }
    return new DefaultPackaging();
  }
}

